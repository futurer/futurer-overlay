# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit desktop xdg udev

DESCRIPTION="Scanner software with more than 7100 drivers"
HOMEPAGE="https://www.hamrick.com/"
SRC_URI="https://www.hamrick.com/files/vuex6497.tgz -> ${P}.tar.gz"

LICENSE="all-rigts-reserved"
SLOT="0"
KEYWORDS="~amd64"

DEPEND="x11-libs/gtk+:2
		x11-libs/gdk-pixbuf:2"
RDEPEND="${DEPEND}"
BDEPEND=""

S="${WORKDIR}/VueScan"

src_install() {
	dobin vuescan
	udev_dorules vuescan.rul
}

pkg_postinst() {
	udev_reload
}

pkg_postrm() {
	udev_reload
}

