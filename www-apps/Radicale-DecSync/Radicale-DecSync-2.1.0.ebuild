# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_10 )

inherit desktop python-single-r1

DESCRIPTION="A Radical storage plugin which adds synchronization with DecSync" 
HOMEPAGE="https://github.com/39aldo39/Radicale-DecSync"
SRC_URI="https://github.com/39aldo39/${PN}/archive/refs/tags/v${PV}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="-amd64"

RDEPEND="${PYTHON_DEPS}
		$(python_gen_cond_dep '
		>=www-apps/radicale-3
		>=dev-libs/libdecsync-2.0.0
		')"
BDEPEND=""
