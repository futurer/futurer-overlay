# Copyright 2021-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_10 )
inherit python-single-r1 desktop xdg

MY_PN=${PN%-bin}
DESCRIPTION="A spaced-repetition memory training program (flash cards)"
HOMEPAGE="https://apps.ankiweb.net/"
SRC_URI="
		https://github.com/ankitects/${MY_PN}/releases/download/${PV}/${MY_PN}-${PV}-linux-qt5.tar.zst -> ${P}.tar.zst
"

LICENSE="AGPL-3"
SLOT="0"
KEYWORDS="-amd64"
REQUIRED_USE="${PYTHON_REQUIRED_USE}"

QA_PREBUILT="/usr/lib/*"
DEPEND="
$(python_gen_cond_dep '
	dev-python/decorator[${PYTHON_USEDEP}]
	dev-python/protobuf-python[${PYTHON_USEDEP}]
	dev-python/distro[${PYTHON_USEDEP}]
	dev-python/beautifulsoup4[${PYTHON_USEDEP}]
	dev-python/requests[${PYTHON_USEDEP}]
	dev-python/flask[${PYTHON_USEDEP}]
	dev-python/waitress[${PYTHON_USEDEP}]
	dev-python/send2trash[${PYTHON_USEDEP}]
	dev-python/markdown[${PYTHON_USEDEP}]
	dev-python/jsonschema[${PYTHON_USEDEP}]
	dev-python/flask-cors[${PYTHON_USEDEP}]
	dev-python/PyQt5[${PYTHON_USEDEP}]
	dev-python/PyQtWebEngine[${PYTHON_USEDEP}]
	dev-python/orjson-bin[${PYTHON_USEDEP}]
	dev-python/stringcase[${PYTHON_USEDEP}]
	')
"

S="${WORKDIR}/${MY_PN}-${PV}-linux-qt5"

RDEPEND="${DEPEND}
	${PYTHON_DEPS}
	!app-misc/anki
"
BDEPEND="app-arch/zstd"

src_unpack() {
	tar --use-compress-program=unzstd -xvf ${DISTDIR}/${P}.tar.zst
}

src_install() {
	python_domodule lib/anki
	python_domodule lib/aqt
	dobin anki
	doicon anki.png
	make_desktop_entry /usr/bin/anki Anki anki Education
}
