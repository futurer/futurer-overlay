# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{10..11} )
inherit desktop xdg

DESCRIPTION="A foss poker tracker in python"
HOMEPAGE="https://github.com/jejellyroll-fr/fpdb-3"
SRC_URI="https://github.com/jejellyroll-fr/fpdb-3/archive/refs/tags/v${PV}-alpha.tar.gz -> ${P}.tar.gz"

LICENSE="AGPL-3"
SLOT="0"
KEYWORDS="~amd64"

DEPEND="
	dev-python/aiohttp
	dev-python/aiosignal
	dev-python/async-timeout
	dev-python/attrs
	dev-python/beautifulsoup4
	dev-python/bs4
	dev-python/certifi
	dev-python/cffi
	dev-python/charset-normalizer
	dev-python/configparser
	dev-python/cycler
	dev-python/fonttools
	dev-python/frozenlist
	dev-python/future
	dev-python/greenlet
	dev-python/idna
	dev-python/kiwisolver
	dev-python/matplotlib
	dev-python/mplfinance
	dev-python/multidict
	dev-python/numpy
	dev-python/packaging
	dev-python/pandas
	dev-python/pillow
	dev-python/pycparser
	dev-python/pyparsing
	dev-python/PyQt5
	dev-python/PyQt5-Qt5
	dev-python/PyQt5-sip
	dev-python/python-dateutil
	dev-python/pytz
	dev-python/QtPy
	dev-python/requests
	dev-python/six
	dev-python/soupsieve
	dev-python/SQLAlchemy
	dev-python/urllib3
	dev-python/xcffib
	dev-python/xlrd
	dev-python/yarl
	dev-python/qdarkstyle
	dev-python/fastapi
"
RDEPEND="${DEPEND}"
BDEPEND=""

S="${WORKDIR}/${PN}"

src_unpack() {
	unpack ${P}.tar.gz
	mv fpdb-3-${PV}-alpha ${PN}
}

src_prepare() {
	default
	mkdir pyfpdb
	mv ./*.py ./pyfpdb/
	mv ./pyfpdb/Database.py ./pyfpdb/DerivedStats.py ./pyfpdb/Hand.py ./pyfpdb/SQL.py ./pyfpdb/WinamaxToFpdb.py ./pyfpdb/setup.py ./pyfpdb/run_fpdb.py ./
}

src_install() {
	dodir /opt/${PN}
	cp -R "${S}/" "${D}/opt/" || die "Install failed!"
	make_desktop_entry "/usr/bin/python /opt/fpdb/run_fpdb.py" fpdb
}
