# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

# autocfg-1.0.1

EAPI=8

PYTHON_COMPAT=( python3_10 )

CRATES="
	adler-0.2.3
	adler32-1.2.0
	alga-0.9.3
	alloc-no-stdlib-2.0.1
	alloc-stdlib-0.2.1
	approx-0.3.2
	autocfg-0.1.7
	autocfg-1.0.1
	bitflags-1.2.1
	brotli-3.3.0
	brotli-decompressor-2.3.1
	byteorder-1.4.2
	bzip2-0.3.3
	bzip2-sys-0.1.10+1.0.8
	cc-1.0.66
	cfg-if-1.0.0
	chrono-0.4.19
	cloudabi-0.0.3
	const_fn-0.4.5
	crc32fast-1.2.1
	crossbeam-channel-0.5.0
	crossbeam-deque-0.8.0
	crossbeam-epoch-0.9.1
	crossbeam-utils-0.8.1
	either-1.6.1
	fasteval-0.2.4
	flate2-1.0.20
	fuchsia-cprng-0.1.1
	generic-array-0.12.3
	getrandom-0.1.16
	hermit-abi-0.1.18
	itoa-0.4.7
	kdtree-0.6.0
	las-0.7.4
	laz-0.5.2
	lazy_static-1.4.0
	libc-0.2.86
	libm-0.2.1
	log-0.4.14
	lzw-0.10.0
	matrixmultiply-0.2.4
	memoffset-0.6.1
	miniz_oxide-0.3.7
	miniz_oxide-0.4.3
	msdos_time-0.1.6
	nalgebra-0.18.1
	num-0.3.1
	num-bigint-0.3.2
	num-complex-0.2.4
	num-complex-0.3.1
	num-integer-0.1.44
	num-iter-0.1.42
	num-rational-0.2.4
	num-rational-0.3.2
	num-traits-0.2.14
	num_cpus-1.13.0
	pdqselect-0.1.0
	pest-2.1.3
	pkg-config-0.3.19
	podio-0.1.7
	ppv-lite86-0.2.10
	proc-macro2-1.0.24
	quote-1.0.8
	rand-0.3.23
	rand-0.4.6
	rand-0.6.5
	rand-0.7.3
	rand_chacha-0.1.1
	rand_chacha-0.2.2
	rand_core-0.3.1
	rand_core-0.4.2
	rand_core-0.5.1
	rand_distr-0.2.2
	rand_hc-0.1.0
	rand_hc-0.2.0
	rand_isaac-0.1.1
	rand_jitter-0.1.4
	rand_os-0.1.3
	rand_pcg-0.1.2
	rand_pcg-0.2.1
	rand_xorshift-0.1.1
	rawpointer-0.2.1
	rayon-1.5.0
	rayon-core-1.9.0
	rdrand-0.4.0
	rstar-0.7.1
	rustc_version-0.3.3
	ryu-1.0.5
	scopeguard-1.1.0
	semver-0.11.0
	semver-parser-0.10.2
	serde-1.0.123
	serde_derive-1.0.123
	serde_json-1.0.64
	statrs-0.9.0
	syn-1.0.60
	thiserror-1.0.26
	thiserror-impl-1.0.26
	time-0.1.44
	typenum-1.12.0
	ucd-trie-0.1.3
	unicode-xid-0.2.1
	uuid-0.8.2
	wasi-0.9.0+wasi-snapshot-preview1
	wasi-0.10.0+wasi-snapshot-preview1
	winapi-0.3.9
	winapi-i686-pc-windows-gnu-0.4.0
	winapi-x86_64-pc-windows-gnu-0.4.0
	zip-0.3.3
		"

inherit cargo python-r1

DESCRIPTION="An advanced geospatial data analysis platform"
HOMEPAGE="https://www.whiteboxgeo.com/ https://github.com/jblindsay/whitebox-tools"
SRC_URI="https://github.com/jblindsay/${PN}/archive/refs/tags/v${PV}.tar.gz
		$(cargo_crate_uris ${CRATES})"

LICENSE="MIT"
SLOT="2.0"
KEYWORDS="-amd64 -arm64 -x86"

DEPEND="${PYTHON_DEPS}"
RDEPEND="${DEPEND}"
BDEPEND="dev-util/rustup"

src_compile() {
	cargo_src_compile --workspace
}
