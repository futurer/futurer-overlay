# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake

DESCRIPTION="Efficient tools for LiDAR processing"
HOMEPAGE="https://github.com/LAStools/LAStools"
SRC_URI="https://github.com/${PN}/${PN}/archive/refs/tags/v${PV}.tar.gz"

LICENSE="LGPL-2.1 all-rights-reserved"
SLOT="0"
KEYWORDS="~amd64"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND="sys-devel/make
		sys-devel/gcc
		dev-util/cmake"

src_install() {
	cmake_src_install
}
