# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="An advanced geospatial data analysis platform"
HOMEPAGE="https://www.whiteboxgeo.com/ https://github.com/jblindsay/whitebox-tools"
SRC_URI="https://www.whiteboxgeo.com/WBT_Linux/WhiteboxTools_linux_amd64.zip"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

S="${WORKDIR}/WBT"

src_configure() {
	mv "${S}"/whitebox_tools "${S}"/whitebox_tools-bin
}

src_install() {
	dobin whitebox_tools-bin
}

pkg_postinst() {
	elog "If whitebox-tools is to be used with QGIS,"
	elog "please update the path in Settings>Options>Processing>Providers"
	elog "to /usr/bin/whitebox-tools-bin"
}
