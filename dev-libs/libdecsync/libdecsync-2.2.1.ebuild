# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="libdecsync is a multiplatform library for synchronizing using DecSync"
HOMEPAGE="https://github.com/39aldo39/libdecsync"
SRC_URI="https://github.com/39aldo39/${PN}/archive/refs/tags/v${PV}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="-amd64"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND="dev-java/gradle-bin"


src_compile() {
	emake
}
