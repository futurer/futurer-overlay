# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit desktop xdg

MERGE_TYPE="binary"
MY_PN=${PN%-bin}

DESCRIPTION="An open source note taking and to-do application"
HOMEPAGE="https://joplinapp.org/"
SRC_URI="https://github.com/laurent22/${MY_PN}/releases/download/v${PV}/Joplin-${PV}.AppImage -> ${P}.AppImage
		https://joplinapp.org/images/Icon512.png -> ${PN}.png
		"

LICENSE="MIT all-rights-reserved"
SLOT="0"
KEYWORDS="-amd64"

DEPEND=""
RDEPEND="
		sys-fs/fuse
		${DEPEND}"
BDEPEND=""

S="${WORKDIR}"


pkg_preinst() {
	chmod +x ${P}.AppImage
}


src_install() {
	doicon ${DISTDIR}/${PN}.png
	insinto /opt/${PN}
	doins ${DISTDIR}/${P}.AppImage
}
